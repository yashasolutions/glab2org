module glab2org

go 1.16

require (
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.3.0
	github.com/spf13/viper v1.10.1
	github.com/xanzy/go-gitlab v0.52.2 // indirect
)
