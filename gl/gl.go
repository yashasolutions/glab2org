package gl

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strings"

	"os"

	"github.com/xanzy/go-gitlab"
)

//GetMyTasks will fetch and print out your own tasks
func GetMyTasks() {

	git, err := gitlab.NewClient(os.Getenv("GL2ORG_TOKEN"),
		gitlab.WithBaseURL(os.Getenv("GL2ORG_URL")))

	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	issues, _, err := git.Issues.ListIssues(&gitlab.ListIssuesOptions{
		State: gitlab.String("opened"),
		Scope: gitlab.String("all"),
		ListOptions: gitlab.ListOptions{
			PerPage: 200,
			Page:    1,
		},
	})

	if err != nil {
		fmt.Println(">> Error")
		fmt.Println(err)
		panic(1)
	}

	fmt.Println("#+title: Gitlab Issues")

	for _, e := range issues {
		fmt.Println("* ISSUE " + e.Title + " :gitlab:")
		fmt.Println("  :PROPERTIES:")
		if e.DueDate != nil {
			fmt.Println("  :DEADLINE: <" + e.DueDate.String() + ">")
		}
		fmt.Println("  :URL: " + e.WebURL)
		fmt.Println("  :END:")
	}
}

//GetMyTasks will fetch and print out your own tasks
func GetProjectTasksNoInFile(projectID string, fname string) {

	git, err := gitlab.NewClient(os.Getenv("GL2ORG_TOKEN"),
		gitlab.WithBaseURL(os.Getenv("GL2ORG_URL")))

	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	b, err := ioutil.ReadFile(fname)

	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	issues, _, err := git.Issues.ListProjectIssues(projectID, &gitlab.ListProjectIssuesOptions{
		State: gitlab.String("opened"),
		ListOptions: gitlab.ListOptions{
			PerPage: 200,
			Page:    1,
		},
	})

	if err != nil {
		fmt.Println(">> Error")
		fmt.Println(err)
		panic(1)
	}

	fmt.Println("#+title: Gitlab Issues")
	fmt.Println("#+filetags: gitlab")

	for _, e := range issues {
		isExist, err := regexp.Match(e.WebURL, b)
		if err != nil {
			panic(err)
		}
		if isExist {
			continue
		}
		DisplayIssue(e)
	}
}

//GetMyTasks will fetch and print out your own tasks
func GetProjectTasks(projectID string) {

	git, err := gitlab.NewClient(os.Getenv("GL2ORG_TOKEN"),
		gitlab.WithBaseURL(os.Getenv("GL2ORG_URL")))

	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	issues, _, err := git.Issues.ListProjectIssues(projectID, &gitlab.ListProjectIssuesOptions{
		State: gitlab.String("opened"),
		ListOptions: gitlab.ListOptions{
			PerPage: 200,
			Page:    1,
		},
	})

	if err != nil {
		fmt.Println(">> Error")
		fmt.Println(err)
		panic(1)
	}

	fmt.Println("#+title: Gitlab Issues")
	fmt.Println("#+filetags: gitlab")

	for _, e := range issues {
		DisplayIssue(e)
	}
}

func DisplayIssue(e *gitlab.Issue) {
	lbs := " "
	for i, l := range e.Labels {
		// make label compliants to orgmode tags
		if i == 0 {
			lbs = lbs + ":"
		}
		m := strings.Replace(l, ",", "_", -1)
		m = strings.Replace(m, "/", "_", -1)
		m = strings.Replace(m, ":", "_", -1)
		m = strings.Replace(m, " ", "_", -1)
		m = strings.ToLower(m)
		lbs = lbs + m + ":"
	}

	fmt.Println("* BACKLOG " + e.Title + "  " + lbs)
	fmt.Println("  :PROPERTIES:")
	if e.DueDate != nil {
		fmt.Println("  :DEADLINE: <" + e.DueDate.String() + ">")
	}
	fmt.Println("  :URL: " + e.WebURL)
	fmt.Println("  :END:")
	fmt.Println(" - [[" + e.WebURL + "][Link to #" + e.ExternalID + "]] ")

}
