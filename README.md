# Gitlab to Org mode

experimental cli to extract gitlab issues to an org file 

## Usage

`glab2org > mytasks.org` to get your own tasks

`glab2org -p=45 > projectID45.org` to get all issues from a given project 

`glab2org -p=45 -f=projectID45.org > newTasks.org` to get all issues not in the file already

## Config

Need to add to environment variables the following:

- `GL2ORG_TOKEN` - gitlab personal access token
- `GL2ORG_URL` - gitlab url (gitlab.com or your own custom domain)

## Roadmap

Not much of a roadmap. Code need some polishing. Ideally I would rather get 
[orglab](https://gitlab.com/yashasolutions/orglab) to work than really expand this one 
but since I am kinda stuck there on learning elisp, I am using this one meanwhile.



